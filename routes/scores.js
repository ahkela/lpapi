const { Score, validateScore } = require("../models/score");
const validate = require("../middleware/validate");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const moment = require("moment");

router.get("/:level", async (req, res) => {
  const scores = await Score.find({ level: req.params.level })
    .sort([
        ["point", "desc"],
        ["completionTime", "asc"],
    ]);
  res.send({data: scores});
});

router.post("/", [validate(validateScore)], async (req, res) => {
  const { level, nickname, point, completionTime } = req.body;
  const score = new Score({
    level,
    nickname,
    point,
    completionTime: moment(completionTime, "HH:mm:ss"),
  });
  await score.save();
  res.send(score);
});

module.exports = router;
