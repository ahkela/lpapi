const express = require('express');
const scores = require('../routes//scores');

module.exports = function(app) {
    app.use(express.json());
    app.use('/api/scores', scores);
}