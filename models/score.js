const Joi = require('joi');
const mongoose = require('mongoose');

const scoreSchema = new mongoose.Schema({
    level: {
        type: Number,
        required: true,
        min: 1,
        max: 3,
    },
    nickname: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50,
    },
    point: {
        type: Number,
        required: true,
        min: 0,
        max: 10,
    },
    completionTime: {
        type: Date,
        required: true,
    }
});

const Score = mongoose.model('Score', scoreSchema);

function validateScore(score) {
    const schema = Joi.object({
        level: Joi.number().required().min(1).max(3),
        nickname: Joi.string().required().min(3).max(50),
        point: Joi.number().required().min(0).max(10),
        completionTime: Joi.string().regex(/(?:[01]\d|2[0123]):(?:[012345]\d):(?:[012345]\d)/),
    });
    return schema.validate(score);
}

exports.Score = Score;
exports.validateScore = validateScore;